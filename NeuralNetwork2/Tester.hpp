#ifndef Tester_hpp
#define Tester_hpp

#include "NeuralNet.hpp"
#include "Module.hpp"
#include <iostream>

using namespace std;

static int n = 0;
void testMatrix();
void testLayer();
void testNeuralNet();
void testModule();

#endif /* Tester_hpp */
