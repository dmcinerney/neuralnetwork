#ifndef Module_hpp
#define Module_hpp

#include "Structure.hpp"

class Module: public Structure{
private:
//Member variables
    //Structure container
    std::vector<Structure> structures;
    
public:
//Friend functions
    friend void testModule();
    
};

#endif /* Module_hpp */
