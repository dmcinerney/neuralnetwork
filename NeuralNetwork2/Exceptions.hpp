#ifndef Exceptions_hpp
#define Exceptions_hpp

#include <exception>

//FIXME: change to runtime exception
//FIXME: split into more specific excpetions or allow one to pass string as input to specify
//FIXME: figure out what's wrong with defining the what function in the cpp file rather than inlining it
class MatrixException : std::exception{
private:
public:
    virtual const char* what() const throw(){return "Matrix Exception";}
};

class LayerException : std::exception{
private:
public:
    virtual const char* what() const throw(){return "Layer Exception";}
};

class NeuralNetException : std::exception{
private:
public:
    virtual const char* what() const throw(){return "NeuralNet Exception";}
};

class StructureException : std::exception{
private:
public:
    virtual const char* what() const throw(){return "NeuralNet Exception";}
};

#endif /* Exceptions_hpp */
