README

Note: This is a project that is still under development and is not complete, but initial tests indicate that it runs relatively well given the right settings.

Class Inheritence Structure:
    Structure                   Layer           Matrix
    /       \
Module      NeuralNet

Class Content Hiarchy:
Module has a Vector of Structures
NeuralNet has a Vector of Layers
Layer has a few different Matrices

Data that it has been tested on:
XOR gate
Digit Recognition (binary between 0 and 1 so far) -- On this data, it scores just around 95 percent when comparing images of just two digits

General PI (possible improvements):
-add Bias node
-add Regularization
-add function that snaps output to 0 or one maybe
-add better optimization
-add graphs to see convergence
-add automatic node deleter/adder
-may want to substitute sigmoid for some sort of linear peicewise function
-alow different activation functions for every neuron
-have varing degrees from batch to stochastic
-could automatically pick number of iterations and degree of stochasticity from number of examples
-possible improvement is to learn a bunch of times from different starting values and pick which one has the lowest costs
-possible improvement is to train more with the examples that have higher cost?
-combine neurons in the same layer that, after training, output the same result

General FIXMEs:
-make sure that copying is not done too much
    may want explicitly define copy assignment operator
-make more exception classes
-change from loops to use iterators
-fix vanishing gradient problem
    Notes: add layers as you go
        pick a better starting point
            unsupervised pretraining - try to predict the input
-may want to research learning rate
-fix bias, it doesn't work

