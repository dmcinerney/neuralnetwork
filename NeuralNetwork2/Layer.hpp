#ifndef Layer_hpp
#define Layer_hpp

#include "Matrix.hpp"
#include <math.h>

class Layer{
private:
//Member variables
    //number of inputs to layer
    int numinputs;
    
    //number of neurons in layer
    int numneurons;
    
    //weights, each row is a neuron, each column is an input to neuron
    Matrix weights;
    
    //used to store weight gradients for training
    Matrix weightGradients;
    
    bool withbias;
    
    //used to store bias for each neuron
    Matrix bias;
    
    //used to store bias gradients for each neuron
    Matrix biasGradients;
    
    //Inputs are the floats that come out of the previous layer's activation function
    //Used to store inputs from previous getOutput call in order to compute gradients
    //  for weights
    Matrix inputs;
    
    //Stimulations are the floats that come out of the multiplication of the inputs
    //  with the weights
    //Used to store stimulations from previous getOutput call to compute gradients for
    //  weights
    Matrix stimulations;
    
//Static member variables
    //Used to determine whether or not to set the seed
    static bool isFirst;
    
//Private functions used only internally by Layer class
    //Sigmoid function
    //input - input value to function
    //returns output as float
    float sigmoid(float x) const;
    
    //Derivative of sigmoid function
    //input - input value to function
    //returns output as float
    float sigmoidDerivative(float x) const;
    
    //Activiation function for every neuron
    //input - input values to neuron
    //returns output values of neuron
    Matrix activation(const Matrix& input) const;
    
    //Derivative of activiation function for every neuron
    //input - input value to neuron
    //returns output values of neuron
    Matrix activationDerivative(const Matrix& input) const;
    
    //Computes random float between low and high
    //returns output
    float randomFloat(float low, float high) const;
    
    //Initializes random weights
    void initWeights();
    
public:
//Friend functions
    friend void testLayer();
    friend void testNeuralNet();
    
//Constructors
    //Default constructor
    Layer();
    
    //Copy constructor
    Layer(const Layer& layer);
    
    //Constructs layer with given specifications
    //Ninputs - number of inputs, Nneurons - number of neurons
    Layer(int Ninputs, int Nneurons);
    
    //Constructs layer from file
    Layer(std::ifstream& file);
    
    //Destructor
    //For now, does nothing
    ~Layer();
    
//Functions used frequently
    //Computes output firing of layer
    //input - activation from previous layer
    //returns result
    Matrix getOutput(const Matrix& input);
    
    //Calculates gradient for each weight and stores in weightGradients
    //Gets error propogation from the last layer
    Matrix calculateWeightGradient(Matrix& errors) throw(LayerException);//NEED TO TEST FURTHER
    
    //Updates all the weights baised on the gradients and the Lrate
    void updateWeights(float Lrate);//NEED TO TEST FURTHER
    
//Other Functions
    //Adds a Neuron to layer, always at last index
    void addNeuron();//NEED TO TEST
    
    //Adds a weight to each Neuron in layer, always at last index
    void addSynapse();//NEED TO TEST
    
    //Removes neuron at index "index" from layer
    void removeNeuron(int index);//NEED TO TEST
    
    //Removes weight at index "index" from layer
    void removeSynapse(int index);//NEED TO TEST
    
    //Prints weights
    void printWeights() const;
    
    //Saves everything important in layer to file:
    //  numinputs, numneurons, and weights
    void saveToFile(std::ofstream& file) const;
    
//Setters and Getters
    //Gets number of inputs
    int getNInputs() const{return this->numinputs;}
    
    //Gets number of outputs
    int getNNeurons() const{return this->numneurons;}
    
    //Sets the gradients to 0
    void resetGradients();//NEED TO TEST FURTHER
    
    //Gets magnitude^2 of the weight gradients for this layer
    float getGradientMagnitude2();//NEED TO IMPLEMENT
    
    //Divides gradient by the magnitude given to normalize gradients throughout NeuralNet
    void normalizeGradient(float magnitude);//NEED TO IMPLEMENT
    
    //NOTE: Getters for all other members are not needed because
    //they are only used for computing the output and gradient
    //internally
};

#endif /* Layer_hpp */
