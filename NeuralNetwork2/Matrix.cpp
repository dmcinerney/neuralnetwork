#include "Matrix.hpp"

//--------------------------------Private functions--------------------------------//
//Used to initialize values for matrix
//If Matrix pointer is null, initialize all values to 0
//If Matrix pointer is not null, then initialize all values
//  to the corresponding values in matrix
void Matrix::initialize(const Matrix* matrix){
    this->values = std::vector< std::vector<float> >(this->getNRows());
    for(int i = 0; i < this->getNRows(); i++){
        this->values[i] = std::vector<float>(this->getNColumns());
        if(matrix != nullptr){
            for(int j = 0; j < this->getNColumns(); j++){
                this->at(i,j) = matrix->at(i,j);
            }
        }
    }
}

//Checks if the input row and column are out of the bounds of the matrix
//Throws an error if they are out of bounds
void Matrix::checkOutOfBounds(int row, int column) const throw(MatrixException){
    if(row >= this->getNRows() || column >= this->getNColumns()){
        std::cout << "Out of Bounds Exception" << std::endl;
        throw MatrixException();
    }
}

//--------------------------------Constructor--------------------------------//
//Default constructor
Matrix::Matrix() : Matrix(0,0) {
    
}

//Copy constructor
Matrix::Matrix(const Matrix& matrix){
    this->numrows = matrix.getNRows();
    this->numcolumns = matrix.getNColumns();
    this->initialize(&matrix);
}

//Constructs matrix with given specifications
//Nrows - number of rows, Ncolumns - number of columns
Matrix::Matrix(int Nrows, int Ncolumns) : numrows(Nrows), numcolumns(Ncolumns){
    initialize(nullptr);
}

//Constructs matrix from file
Matrix::Matrix(std::ifstream& file){
    file.read((char*)&this->numrows, sizeof(float));
    file.read((char*)&this->numcolumns, sizeof(float));
    this->initialize(nullptr);
    for(int i = 0; i < this->getNRows(); i++){
        for(int j = 0; j < this->getNColumns(); j++){
            file.read((char*)&this->at(i,j), sizeof(float));
        }
    }
}

//Destructor
//For now, does nothing
Matrix::~Matrix(){
    
}

//--------------------------------Main functions--------------------------------//
//Loops over all elements and performs function(element, number) and
//puts results into a matrix it returns by value
Matrix Matrix::forAllElements(float number, float (*function)(const float&, const float&)) const{
    Matrix matrix = Matrix(this->getNRows(),this->getNColumns());
    for(int i = 0; i < this->getNRows(); i++){
        for(int j = 0; j < this->getNColumns(); j++){
            matrix.at(i,j) = function(this->at(i,j),number);
        }
    }
    return matrix;
}

//Loops over all elements and performs function(element, element of matrix parameter) and
//puts results into a matrix it returns by value
//Throws an error if the input matrix doesn't have the same dimensions as this
Matrix Matrix::forAllElements(const Matrix& m, float (*function)(const float&, const float&)) const throw(MatrixException){
    if(m.getNRows() != this->getNRows() || m.getNColumns() != this->getNColumns()){
        std::cout << "matricies are not of the same dimensions!" << std::endl;
        throw MatrixException();
    }
    Matrix matrix = Matrix(this->getNRows(),this->getNColumns());
    for(int i = 0; i < this->getNRows(); i++){
        for(int j = 0; j < this->getNColumns(); j++){
            matrix.at(i,j) = function(this->at(i,j),m.at(i,j));
        }
    }
    return matrix;
}

//Gets float at given row and column
//returns float by const reference
//Neither the function itself or that which calls it can change object
//Throws an error if row or column are out of bounds
const float& Matrix::at(int row, int column) const throw(MatrixException){
    checkOutOfBounds(row, column);
    return this->values[row][column];
}

//Gets float at given row and column
//returns float by reference
//The function that calls it may change object using the returned reference
//Throws an error if row or column are out of bounds
float& Matrix::at(int row, int column) throw(MatrixException){
    checkOutOfBounds(row, column);
    return this->values[row][column];
}

//Element-wise addition
//Throws an error if input matrix is not the same dimensions as this
Matrix Matrix::operator+(const Matrix& m) const throw(MatrixException){
    return forAllElements(m,addition);
}

//Element-wise subtraction
//Throws an error if input matrix is not the same dimensions as this
Matrix Matrix::operator-(const Matrix& m) const throw(MatrixException){
    return forAllElements(m,subtraction);
}

//Element-wise multiplication
//Throws an error if input matrix is not the same dimensions as this
Matrix Matrix::multiply(const Matrix& m) const throw(MatrixException){
    return forAllElements(m,multiplication);
}

//Element-wise division
//Throws an error if input matrix is not the same dimensions as this
Matrix Matrix::divide(const Matrix& m) const throw(MatrixException){
    return forAllElements(m,division);
}

//Scalar multiplication
Matrix Matrix::operator*(const float scalar) const{
    return forAllElements(scalar,multiplication);
}

//Scalar division
Matrix Matrix::operator/(const float scalar) const{
    return forAllElements(scalar,division);
}

//Matrix multiplication
//FIXME: may want to change implementation to be like numpy
//Throws an error if dimensions of input matrix are not compatible
Matrix Matrix::dot(const Matrix& m) const throw(MatrixException){
    if(this->getNColumns() != m.getNRows()){
        std::cout << "Cannot do matrix multiplication!" << std::endl;
        std::cout << this->getNRows() << " by " << this->getNColumns() << " times " << m.getNRows() << " by " << m.getNColumns() << std::endl;
        throw MatrixException();
    }
    Matrix matrix = Matrix(this->getNRows(), m.getNColumns());
    for(int i = 0; i < matrix.getNRows(); i++){
        for(int j = 0; j < matrix.getNColumns(); j++){
            for(int index = 0; index < this->getNColumns(); index++){
                matrix.at(i,j) += this->values[i][index]*m.at(index,j);
            }
        }
    }
    return matrix;
}

//--------------------------------Other functions--------------------------------//
//Adds row at end of matrix
void Matrix::addRow(){
    this->values.push_back(std::vector<float>(this->getNColumns()));
    this->numrows++;
}

//Adds column at end of matrix
void Matrix::addColumn(){
    for(int i = 0; i < this->getNRows(); i++){
        this->values[i].push_back(0);
    }
    this->numcolumns++;
}

//Removes row at given index
//Throws an error if index is out of bounds
void Matrix::removeRow(int index) throw(MatrixException){
    //If out of range, throw Exception
    if(index >= this->getNRows()){
        std::cout << "Out of range! Tried to remove row at index " << index << ".  Only " << this->getNRows() << " rows." << std::endl;
        exit(1);//FIXME: Change to Exception
    }
    this->values.erase(this->values.begin()+index);
    this->numrows--;
}

//Removes column at given index
//Throws an error if index is out of bounds
void Matrix::removeColumn(int index) throw(MatrixException){
    if(index >= this->getNColumns()){
        std::cout << "Out of range! Tried to remove row at index " << index << ".  Only " << this->getNColumns() << " columns." << std::endl;
        exit(1);//FIXME: Change to Exception
    }
    for(int i = 0; i < this->getNRows(); i++){
        this->values[i].erase(this->values[i].begin()+index);
    }
    this->numcolumns--;
}

//Saves all member variables to file
void Matrix::saveToFile(std::ofstream& file) const{
    file.write((char*)&this->numrows, sizeof(float));
    file.write((char*)&this->numcolumns, sizeof(float));
    for(int i = 0; i < this->getNRows(); i++){
        for(int j = 0; j < this->getNColumns(); j++){
            file.write((char*)&this->at(i,j), sizeof(float));
        }
    }
}

//--------------------------------Setters and Getters--------------------------------//
//Creates transpose of matrix
//returns by value
Matrix Matrix::getTranspose() const{
    Matrix matrix = Matrix(this->getNColumns(),this->getNRows());
    for(int i = 0; i < matrix.getNRows(); i++){
        for(int j = 0; j < matrix.getNColumns(); j++){
            matrix.at(i,j) = this->at(j,i);
        }
    }
    return matrix;
}

//Gets string printout of whole matrix
std::string Matrix::getString() const{
    std::string matrixstring = "";
    for(int i = 0; i < this->getNRows(); i++){
        for(int j = 0; j < this->getNColumns(); j++){
            if(this->at(i,j)>=0){
                matrixstring += " ";
            }
            matrixstring += std::to_string(this->at(i,j))+"    ";
        }
        matrixstring += "\n";
    }
    return matrixstring;
}

//--------------------------------Non-member functions--------------------------------//
//Float addition
float addition(const float& x, const float& y){return x+y;}

//Float subtraction
float subtraction(const float& x, const float& y){return x-y;}

//Float multiplication
float multiplication(const float& x, const float& y){return x*y;}

//Float division
float division(const float& x, const float& y){return x/y;}