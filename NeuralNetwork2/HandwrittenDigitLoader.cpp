#include "HandwrittenDigitLoader.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>

void loadData(vector<Matrix>& inputs, vector<Matrix>& outputs){
    cout << "Loading data " << endl;
    std::ifstream infile("/Users/jeredmcinerney/Documents/workspaceXcode/NeuralNetwork2/NeuralNetwork2/train.csv");
    int linenum = 0;
    string line;
    int featurenum = 0;
    while (std::getline(infile, line)){
        if(linenum != 0){
            inputs.push_back(Matrix(featurenum,1));
            outputs.push_back(Matrix(1,1));
        }
        char* number = strtok((char*)line.c_str(), ",");
        featurenum = 0;
        while (number != NULL){
            if(linenum != 0){
                float num = atof(number);
                if(featurenum == 0){
                    outputs[linenum-1].at(0,0) = num;
                } else {
                    inputs[linenum-1].at(featurenum,0) = num;
                }
            }
            featurenum++;
            number = strtok(NULL, ",");
        }
        linenum++;
        if((linenum-1) % 1000 == 0 && linenum != 1){
            cout << linenum << endl;
        }
        if((linenum-1) == 5000){
            break;
        }
    }
    cout << "number of lines: " << linenum << endl;
}
