#include "Tester.hpp"
#include "HandwrittenDigitLoader.hpp"
#include <time.h>

//FIXME: make sure that copying is not done too much
//          may want explicitly define copy assignment operator
//FIXME: make more exceptions
//FIXME: change for loops to use iterators
//FIXME: fix vanishing gradient problem
    //Notes: add layers as you go
    //  pick a better starting point
    //      unsupervised pretraining - try to predict the input
//PI: may want to substitute sigmoid for linear peicewise thingy
//PI: alow different activation functions for every neuron
//FIXME: may want to research learning rate
//FIXME: fix bias, it doesn't work
//PI: have varing degrees from batch to stochastic
//PI: could automatically pick number of iterations and degree of stochasticity from number of examples
//PI: possible improvement is to learn a bunch of times from different starting values and pick which one has the lowest costs
//PI: possible improvement is to train more with the examples that have higher cost?
//PI: combine neurons in the same layer that, after training, output the same result
int main(){
    cout << "Welcome to the Neural Network Constructor!" << endl;
    /*testMatrix();
    cout << endl;
    testLayer();
    cout << endl;
    */
    clock_t t1,t2;
    t1=clock();
    testNeuralNet();
    t2=clock();
    float diff (((float)t2-(float)t1)/CLOCKS_PER_SEC);
    cout << "Running time: " << diff << " seconds." << endl << endl;
}

//-------------------------------- --------------------------------//
