#include "Tester.hpp"
#include <fstream>
#include "HandwrittenDigitLoader.hpp"

void testMatrix(){
    cout << "TESTING MATRIX" << endl;
    cout << endl << endl;
    
    cout << "creating matrix" << endl;
    Matrix m1 = Matrix();
    m1.addRow();
    m1.addColumn();
    cout << m1.getString() << endl;
    cout << "creating matrix" << endl;
    Matrix matrix = Matrix(2,4);
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 4; j++){
            matrix.at(i,j) = (i+1)*(j+1)+j;
        }
    }
    cout << matrix.getString() << endl;
    cout << endl << endl;
    
    cout << "matrix copy" << endl;
    Matrix matrixcopy = Matrix(matrix);
    cout << matrixcopy.getString() << endl;
    matrix.addRow();
    cout << matrix.getString() << endl;
    matrix.addColumn();
    cout << matrix.getString() << endl;
    cout << endl << endl;
    
    cout << "testing operations" << endl;
    cout << "addition of matrix to itself" << endl;
    matrix = matrix+matrix;
    cout << matrix.getString() << endl;
    cout << "subtraction of matrix/2" << endl;
    matrix = matrix-matrix/2;
    cout << matrix.getString() << endl;
    cout << "multiplication of matrix by itself" << endl;
    matrix = matrix.multiply(matrix);
    cout << matrix.getString() << endl;
    cout << "multiplication of matrix by 2" << endl;
    matrix = matrix*2;
    cout << matrix.getString() << endl;
    cout << "getting transpose" << endl;
    Matrix matrixTranspose = matrix.getTranspose();
    cout << matrixTranspose.getString() << endl;
    cout << "matrix multiplying matrix by transpose" << endl;
    Matrix matrixmult = matrix.dot(matrixTranspose);
    Matrix matrixtemp = Matrix(matrixmult);
    cout << matrixmult.getString() << endl;
    cout << endl << endl;
    
    cout << "testing removes and adds" << endl;
    cout << " - row 2" << endl;
    matrixmult.removeRow(1);
    cout << matrixmult.getString() << endl;
    cout << " - column 2" << endl;
    matrixmult.removeColumn(1);
    cout << matrixmult.getString() << endl;
    cout << endl << endl;
    
    cout << "testing exceptions" << endl;
    cout << "addition exception" << endl;
    try{
        matrixtemp+matrixmult;
    } catch (MatrixException& e){
        cout << e.what() << endl;
    }
    cout << "subtraction exception" << endl;
    try{
        matrixtemp-matrixmult;
    } catch (MatrixException& e){
        cout << e.what() << endl;
    }
    cout << endl << endl;
    
    cout << "testing matrix multiplication" << endl;
    cout << "matrix multiplying transpose by matrix" << endl;
    Matrix matrixmult2 = matrixTranspose.dot(matrix);
    cout << matrixmult2.getString() << endl;
    cout << "matrix multiplying matrix by matrix" << endl;
    try{
        matrix.dot(matrix);
    } catch (MatrixException& e){
        cout << e.what() << endl;
    }
    cout << endl << endl;
    
    cout << "testing file stuff" << endl;
    cout << "matrix before file save:" << endl;
    cout << matrixmult2.getString() << endl;
    cout << "saving to file Matrix.bin" << endl;
    ofstream file("Matrix.bin", ios::out | ios::binary);
    matrixmult2.saveToFile(file);
    file.close();
    cout << "getting matrix from file" << endl;
    ifstream newfile("Matrix.bin", ios::in | ios::binary);
    Matrix newmatrix = Matrix(newfile);
    newfile.close();
    if( remove( "Matrix.bin" ) != 0 )
        perror( "error deleting file" );
    else
        puts( "file successfully deleted" );
    cout << "matrix from file:" << endl;
    cout << newmatrix.getString() << endl;
    cout << endl << endl;
    
    cout << "DONE TESTING MATRIX" << endl;
    cout << endl << endl;
}

void testLayer(){
    cout << "TESTING LAYER" << endl;
    cout << endl << endl;
    
    cout << "creating layer" << endl;
    Layer layer = Layer(3,3);
    Layer layer2 = Layer(3,3);
    cout << endl << endl;
    
    cout << "testing sigmoid function" << endl;
    float number = 5;
    cout << "sigmoid(" << number << ") is " << layer.sigmoid(number) << endl;
    cout << endl << endl;
    
    cout << "testing sigmoid derivative function" << endl;
    cout << "sigmoidDerivative(" << number << ") is " << layer.sigmoidDerivative(number) << endl;
    cout << endl << endl;
    
    cout << "testing activation function" << endl;
    Matrix matrix = Matrix(2,4);
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 4; j++){
            matrix.at(i,j) = (i+1)*(j+1)+j;
        }
    }
    Matrix matrix2 = Matrix(matrix);
    cout << matrix.getString() << endl;
    matrix = layer.activation(matrix);
    cout << matrix.getString() << endl;
    cout << endl << endl;
    
    cout << "testing activationDerivative function" << endl;
    cout << matrix2.getString() << endl;
    matrix2 = layer.activationDerivative(matrix2);
    cout << matrix2.getString() << endl;
    cout << endl << endl;
    
    cout << "printing random weights" << endl;
    layer.printWeights();
    layer2.printWeights();
    for(int i = 0; i < 3; i++){
        layer.stimulations.at(i,0) = 2*(i+1);
        for(int j = 0; j < 3; j++){
            if(i == 0){
                layer.inputs.at(j,0) = 2*(i+1)*(j+1)+j;
            }
            layer.weightGradients.at(i,j) = 2*(i+1)*(j+1)+j;
        }
    }
    cout << endl << endl;
    
    cout << "testing copy constructor" << endl;
    Layer layercopy = Layer(layer);
    cout << "layer" << endl;
    cout << "weights: " << endl << layer.weights.getString() << endl;
    cout << "weightGradients: " << endl << layer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layer.inputs.getString() << endl;
    cout << "stimulations: " << endl << layer.stimulations.getString() << endl;
    cout << "layercopy" << endl;
    cout << "weights: " << endl << layercopy.weights.getString() << endl;
    cout << "weightGradients: " << endl << layercopy.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layercopy.inputs.getString() << endl;
    cout << "stimulations: " << endl << layercopy.stimulations.getString() << endl;
    cout << endl << endl;
    
    cout << "testing removes and adds" << endl;
    cout << "weights: " << endl << layer.weights.getString() << endl;
    cout << "weightGradients: " << endl << layer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layer.inputs.getString() << endl;
    cout << "stimulations: " << endl << layer.stimulations.getString() << endl;
    cout << "addNeuron" << endl;
    layer.addNeuron();
    cout << "weights: " << endl << layer.weights.getString() << endl;
    cout << "weightGradients: " << endl << layer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layer.inputs.getString() << endl;
    cout << "stimulations: " << endl << layer.stimulations.getString() << endl;
    cout << "addSynapse" << endl;
    layer.addSynapse();
    cout << "weights: " << endl << layer.weights.getString() << endl;
    cout << "weightGradients: " << endl << layer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layer.inputs.getString() << endl;
    cout << "stimulations: " << endl << layer.stimulations.getString() << endl;
    cout << "removeNeuron" << endl;
    layer.removeNeuron(1);
    cout << "weights: " << endl << layer.weights.getString() << endl;
    cout << "weightGradients: " << endl << layer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layer.inputs.getString() << endl;
    cout << "stimulations: " << endl << layer.stimulations.getString() << endl;
    cout << "removeSynapse" << endl;
    layer.removeSynapse(1);
    cout << "weights: " << endl << layer.weights.getString() << endl;
    cout << "weightGradients: " << endl << layer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layer.inputs.getString() << endl;
    cout << "stimulations: " << endl << layer.stimulations.getString() << endl;
    cout << endl << endl;
    
    cout << "testing getOutput" << endl;
    Layer layer3 = Layer(4,3);
    cout << "weights: " << endl;
    layer3.printWeights();
    Matrix matrix3 = Matrix(4,1);
    for(int i = 0; i < 4; i++){
        matrix3.at(i,0) = i;
    }
    cout << "input: " << endl << matrix3.getString() << endl;
    cout << "output: " << endl << layer3.getOutput(matrix3).getString() << endl;
    cout << endl << endl;
    
    cout << "testing file stuff" << endl;
    cout << "layer before file save:" << endl;
    cout << "weights: " << endl << layer.weights.getString() << endl;
    cout << "weightGradients: " << endl << layer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << layer.inputs.getString() << endl;
    cout << "stimulations: " << endl << layer.stimulations.getString() << endl;
    cout << "saving to file Layer.bin" << endl;
    ofstream file("Layer.bin", ios::out | ios::binary);
    layer.saveToFile(file);
    file.close();
    cout << "getting layer from file" << endl;
    ifstream newfile("Layer.bin", ios::in | ios::binary);
    Layer newlayer = Layer(newfile);
    newfile.close();
    if( remove( "Layer.bin" ) != 0 )
        perror( "error deleting file" );
    else
        puts( "file successfully deleted" );
    cout << "matrix from file:" << endl;
    cout << "weights: " << endl << newlayer.weights.getString() << endl;
    cout << "weightGradients: " << endl << newlayer.weightGradients.getString() << endl;
    cout << "inputs: " << endl << newlayer.inputs.getString() << endl;
    cout << "stimulations: " << endl << newlayer.stimulations.getString() << endl;
    cout << endl << endl;
    
    cout << "DONE TESTING LAYER" << endl;
    cout << endl << endl;
}

Matrix convertto4binary(const Matrix& mat1by1){
    Matrix mat4by1 = Matrix(1,1);
    float num = mat1by1.at(0,0);
    /*
    if(num > 7){
        mat4by1.at(0,0) = 1;
        num -= 8;
    }
    if(num > 3){
        mat4by1.at(1,0) = 1;
        num -= 4;
    }
    if(num > 1){
        mat4by1.at(2,0) = 1;
        num -= 2;
    }
    */
    if(num > n){
        mat4by1.at(0,0) = 1;
    }
    return mat4by1;
}
Matrix convertto10binary(const Matrix& mat1by1){
    Matrix mat10by1 = Matrix(2,1);
    float num = mat1by1.at(0,0);
    if(num == 2){
        mat10by1.at(0,0) = 1;
        mat10by1.at(1,0) = 1;
    } else {
        mat10by1.at(num,0) = 1;
    }
    return mat10by1;
}

void testNeuralNet(){
    cout << "TESTING NEURALNET" << endl;
    cout << endl << endl;
    /*
    cout << "creating NeuralNet" << endl;
    NeuralNet neuralnet = NeuralNet(1,10,3,3);
    cout << endl << endl;
    
    Matrix inputs = Matrix(3,1);
    Matrix outputs = Matrix(3,1);
    inputs.at(0,0) = .1; inputs.at(1,0) = .2; inputs.at(2,0) = .3;
    outputs.at(0,0) = .3; outputs.at(1,0) = .2; outputs.at(2,0) = .1;
    neuralnet.forward(inputs);
    cout << "estimate: " << endl << neuralnet.estimate.getString() << endl;
    cout << "cost : " << neuralnet.cost(outputs) << endl;
    cout << endl << endl;
    neuralnet.costDerivative(outputs);
    cout << "weights: " << endl << neuralnet.layers[0].weights.getString() << endl;
    cout << "weightGradients: " << endl << neuralnet.layers[0].weightGradients.getString() << endl;
    cout << "inputs: " << endl << neuralnet.layers[0].inputs.getString() << endl;
    cout << "stimulations: " << endl << neuralnet.layers[0].stimulations.getString() << endl;
    neuralnet.updateWeights(1);
    neuralnet.forward(inputs);
    cout << "estimate: " << endl << neuralnet.estimate.getString() << endl;
    cout << "cost: " << neuralnet.cost(outputs) << endl;
    cout << endl << endl;
    for(int i = 0; i < 100; i++){
        neuralnet.resetGradients();
        neuralnet.costDerivative(outputs);
        neuralnet.updateWeights(1);
        neuralnet.forward(inputs);
        cout << "estimate: " << endl << neuralnet.estimate.getString() << endl;
        cout << "cost: " << neuralnet.cost(outputs) << endl;
        cout << endl;
    }
    cout << endl << endl;
    
    vector<Matrix> inputs2 = vector<Matrix>(3);
    vector<Matrix> outputs2 = vector<Matrix>(3);
    for(int i = 0; i < 3; i++){
        inputs2[i] = Matrix(1,1);
        inputs2[i].at(0,0) = (i+1.)/10;
        outputs2[i] = Matrix(1,1);
        outputs2[i].at(0,0) = (i+1.)/10;
    }
    NeuralNet neuralnet2 = NeuralNet(2,10,1,1);
    neuralnet2.train(inputs2, outputs2, 10000);
    for(int i = 0; i < 3; i++){
        neuralnet2.forward(inputs2[i]);
        cout << "input: " << endl << inputs2[i].getString();
        cout << "estimate: " << endl << neuralnet2.estimate.getString() << endl;
        cout << "cost: " << neuralnet2.cost(outputs2[i]) << endl;
    }
    cout << endl << endl;
    */
    vector<Matrix> inputs3 = vector<Matrix>(4);
    vector<Matrix> outputs3 = vector<Matrix>(4);
    for(int i = 0; i < 4; i++){
        inputs3[i] = Matrix(2,1);
        outputs3[i] = Matrix(1,1);
    }
    inputs3[0].at(0,0) = 0; inputs3[0].at(1,0) = 0; //inputs3[0].at(2,0) = 0;
    outputs3[0].at(0,0) = 0;
    inputs3[1].at(0,0) = 0; inputs3[1].at(1,0) = 1; //inputs3[1].at(2,0) = 0;
    outputs3[1].at(0,0) = 1;
    inputs3[2].at(0,0) = 1; inputs3[2].at(1,0) = 0; //inputs3[2].at(2,0) = 0;
    outputs3[2].at(0,0) = 1;
    inputs3[3].at(0,0) = 1; inputs3[3].at(1,0) = 1; //inputs3[3].at(2,0) = 0;
    outputs3[3].at(0,0) = 0;
    /*
    NeuralNet neuralnet3 = NeuralNet(2,4,3,1);
    
    neuralnet3.layers[0].weights.at(0,0) = 10; neuralnet3.layers[0].weights.at(0,1) = 0; neuralnet3.layers[0].weights.at(0,2) = -5;
    neuralnet3.layers[0].weights.at(1,0) = 0; neuralnet3.layers[0].weights.at(1,1) = 10; neuralnet3.layers[0].weights.at(1,2) = -5;
    neuralnet3.layers[0].weights.at(2,0) = 0; neuralnet3.layers[0].weights.at(2,1) = 0; neuralnet3.layers[0].weights.at(2,2) = 5;
    neuralnet3.layers[0].weights.at(3,0) = 10; neuralnet3.layers[0].weights.at(3,1) = 10; neuralnet3.layers[0].weights.at(3,2) = -15;
    
    neuralnet3.layers[1].weights.at(0,0) = 10; neuralnet3.layers[1].weights.at(0,1) = 10; neuralnet3.layers[1].weights.at(0,2) = -5; neuralnet3.layers[1].weights.at(0,3) = -20;
    
    for(int i = 0; i < 4; i++){
        neuralnet3.forward(inputs3[i]);
        cout << "input: " << endl << inputs3[i].getString();
        cout << "estimate: " << endl << neuralnet3.estimate.getString() << endl;
        cout << "cost: " << neuralnet3.cost(outputs3[i]) << endl;
    }
    neuralnet3.layers[0].printWeights();
    neuralnet3.layers[1].printWeights();
    neuralnet3.train(inputs3, outputs3, 1000);
    neuralnet3.layers[0].printWeights();
    neuralnet3.layers[1].printWeights();
    for(int i = 0; i < 4; i++){
        neuralnet3.forward(inputs3[i]);
        cout << "input: " << endl << inputs3[i].getString();
        cout << "estimate: " << endl << neuralnet3.estimate.getString() << endl;
        cout << "cost: " << neuralnet3.cost(outputs3[i]) << endl;
    }
    cout << endl << endl;
    */
    
    //NeuralNet neuralnet4 = NeuralNet(2,3,2,1,1);
    /*
    neuralnet4.layers[0].weights.at(0,0) = 10; neuralnet4.layers[0].weights.at(0,1) = 10; neuralnet4.layers[0].weights.at(0,2) = -5;
    neuralnet4.layers[0].weights.at(1,0) = 10; neuralnet4.layers[0].weights.at(1,1) = 10; neuralnet4.layers[0].weights.at(1,2) = -15;
    neuralnet4.layers[0].weights.at(2,0) = 0; neuralnet4.layers[0].weights.at(2,1) = 0; neuralnet4.layers[0].weights.at(2,2) = 5;
    
    neuralnet4.layers[1].weights.at(0,0) = 10; neuralnet4.layers[1].weights.at(0,1) = -10; neuralnet4.layers[1].weights.at(0,2) = -5;
    */
    /*
    for(int i = 0; i < 4; i++){
        neuralnet4.forward(inputs3[i]);
        cout << "input: " << endl << inputs3[i].getString();
        cout << "estimate: " << endl << neuralnet4.estimate.getString() << endl;
        cout << "cost: " << neuralnet4.cost(outputs3[i]) << endl;
    }
    neuralnet4.layers[0].printWeights();
    neuralnet4.layers[1].printWeights();
    neuralnet4.train(inputs3, outputs3, 200);
    neuralnet4.layers[0].printWeights();
    neuralnet4.layers[1].printWeights();
    for(int i = 0; i < 4; i++){
        neuralnet4.forward(inputs3[i]);
        cout << "input: " << endl << inputs3[i].getString();
        cout << "estimate: " << endl << neuralnet4.estimate.getString() << endl;
        cout << "cost: " << neuralnet4.cost(outputs3[i]) << endl;
    }
    cout << endl << endl;
    neuralnet4.test(inputs3, outputs3, "binary");
    
    */
    clock_t t1,t2;
    vector<Matrix> inputs = vector<Matrix>();
    vector<Matrix> outputs = vector<Matrix>();
    vector<Matrix> inputsdev = vector<Matrix>();
    vector<Matrix> outputsdev = vector<Matrix>();
    vector<Matrix> inputs1 = vector<Matrix>();
    vector<Matrix> outputs1 = vector<Matrix>();
    loadData(inputs1, outputs1);
    for(int i = 0; i < outputs1.size(); i++){
        if(outputs1[i].at(0,0) == n || outputs1[i].at(0,0) == n+1 /*|| outputs1[i].at(0,0) == 2*/){
            if(i < 3*outputs1.size()/4){
                inputs.push_back(inputs1[i]);
                outputs.push_back(Matrix(convertto4binary(outputs1[i])));
            } else {
                inputsdev.push_back(inputs1[i]);
                outputsdev.push_back(Matrix(convertto4binary(outputs1[i])));
            }
        }
    }
    int i = 0;
    int array[2];
    cout << inputs[i].getNRows() << endl;
    array[0] = 100; array[1] = 10;
    NeuralNet neuralnet = NeuralNet(2, array/*inputs[i].getNRows()*/, inputs[i].getNRows(), outputs[0].getNRows(), 1);
    neuralnet.test(inputs, outputs, "binary");
    cout << "output: " << outputs[i].getString() << endl;
    neuralnet.forward(inputs[i]);
    cout << "estimate: " << neuralnet.estimate.getString() << endl;
    cout << "cost: " << neuralnet.cost(outputs[i]) << endl;
    t1=clock();
    neuralnet.train(inputs, outputs, 2);//Neet to alter number of iterations depending on how many examples.
    t2=clock();
    float diff (((float)t2-(float)t1)/CLOCKS_PER_SEC);
    //cout << neuralnet.layers[0].weights.getString() << endl;
    neuralnet.test(inputs, outputs, "binary");
    //cout << "input: " << inputs[i].getString() << endl;
    cout << "output: " << outputs[i].getString() << endl;
    neuralnet.forward(inputs[i]);
    cout << "estimate: " << neuralnet.estimate.getString() << endl;
    cout << "cost: " << neuralnet.cost(outputs[i]) << endl;
    neuralnet.test(inputsdev, outputsdev, "binary");
    
    cout << "Running time of training: " << diff << " seconds." << endl << endl;
    
    cout << "DONE TESTING NEURALNET" << endl;
    cout << endl << endl;
}
