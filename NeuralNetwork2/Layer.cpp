#include "Layer.hpp"
#include <cstdlib>
#include <ctime>
#include <cmath>

//--------------------------------Initialize static member variables--------------------------------//
bool Layer::isFirst = true;

//--------------------------------Private functions--------------------------------//
//Sigmoid function
//input - input value to function
//returns output as float
float Layer::sigmoid(float x) const{
    return 1./(1.+exp(-x));
}

//Derivative of sigmoid function
//input - input value to function
//returns output as float
float Layer::sigmoidDerivative(float x) const{
    float y = exp(-x)/pow(1.+exp(-x),2);
    if(isnan(y)){
        y = 0;
    }
    return y;
}

//Activiation function for every neuron
//input - input values to neuron
//returns output values of neuron
Matrix Layer::activation(const Matrix& input) const{
    Matrix matrix = Matrix(input.getNRows(), input.getNColumns());
    for(int i = 0; i < input.getNRows(); i++){
        for(int j = 0; j < input.getNColumns(); j++){
            matrix.at(i,j) = this->sigmoid(input.at(i,j)+this->bias.at(i,j));
        }
    }
    return matrix;
}

//Derivative of activiation function for every neuron
//input - input value to neuron
//returns output values of neuron
Matrix Layer::activationDerivative(const Matrix& input) const{
    Matrix matrix = Matrix(input.getNRows(), input.getNColumns());
    for(int i = 0; i < input.getNRows(); i++){
        for(int j = 0; j < input.getNColumns(); j++){
            matrix.at(i,j) = this->sigmoidDerivative(input.at(i,j)+this->bias.at(i,j));
        }
    }
    return matrix;
}

//Computes random float between low and high
//returns output
float Layer::randomFloat(float low, float high) const{
    float randfrom0to1 = (float)rand()/(float)RAND_MAX;
    return low + randfrom0to1*(high-low);
}

//Initializes random weights
void Layer::initWeights(){
    for(int i = 0; i < this->weights.getNRows(); i++){
        for(int j = 0; j < this->weights.getNColumns(); j++){
            //this->weights.at(i,j) = ((rand()%2)*2-1)*randomFloat(2,4);
            this->weights.at(i,j) = randomFloat(-5,5);
        }
        this->bias.at(i,0) = 0;
    }
}

//--------------------------------Constructors--------------------------------//
//Default constructor
Layer::Layer() : Layer(0,0){
    
}

//Copy constructor
Layer::Layer(const Layer& layer){
    this->numinputs = layer.getNInputs();
    this->numneurons = layer.getNNeurons();
    this->weights = Matrix(layer.weights);
    this->weightGradients = Matrix(layer.weightGradients);
    this->bias = Matrix(layer.bias);
    this->biasGradients = Matrix(layer.biasGradients);
    this->inputs = Matrix(layer.inputs);
    this->stimulations = Matrix(layer.stimulations);
}

//Constructs layer with given specifications
//Ninputs - number of inputs, Nneurons - number of neurons
Layer::Layer(int Ninputs, int Nneurons) :
        numinputs(Ninputs),
        numneurons(Nneurons),
        weights(Nneurons,Ninputs),
        weightGradients(Nneurons,Ninputs),
        bias(Nneurons,1),
        biasGradients(Nneurons,1),
        inputs(Ninputs,1),
        stimulations(Nneurons,1){
    if(this->isFirst){
        srand(static_cast<unsigned>(time(0)));
        rand();
        this->isFirst = 0;
    }
    this->initWeights();
    this->withbias = false;
}

//Constructs layer from file
Layer::Layer(std::ifstream& file){
    file.read((char*)&this->numinputs, sizeof(float));
    file.read((char*)&this->numneurons, sizeof(float));
    this->weights = Matrix(file);
    this->weightGradients = Matrix(file);
    this->bias = Matrix(file);
    this->biasGradients = Matrix(file);
    this->inputs = Matrix(file);
    this->stimulations = Matrix(file);
}

//Destructor
//For now, does nothing
Layer::~Layer(){
    
}

//--------------------------------Main functions--------------------------------//
//Computes output firing of layer
//input - activation from previous layer
//returns result
Matrix Layer::getOutput(const Matrix& input){
    this->inputs = Matrix(input);
    //multiply weights by input
    this->stimulations = this->weights.dot(input);
    //run through activation function
    return this->activation(this->stimulations);
}

//Calculates gradient for each weight and stores in weightGradients
//Gets error propogation from the last layer
Matrix Layer::calculateWeightGradient(Matrix& errors) throw(LayerException){
    if(errors.getNRows() != this->numneurons && errors.getNColumns() != 1){
        std::cout << "throw error!!!" << std::endl;
        throw LayerException();
    }
    Matrix delta = errors.multiply(this->activationDerivative(stimulations));
    this->weightGradients = this->weightGradients+this->inputs.dot(delta.getTranspose()).getTranspose();
    if(this->withbias) this->biasGradients = this->biasGradients+errors;
    return this->weights.getTranspose().dot(delta);
}

//Updates all the weights baised on the gradients and the Lrate
void Layer::updateWeights(float Lrate){
    this->weights = this->weights-this->weightGradients*Lrate;
    if(this->withbias) this->bias = this->bias-this->biasGradients*Lrate;
    //std::cout << "Layer Gradients : " << (this->weightGradients*Lrate).getString() << std::endl;
}

//--------------------------------Other Functions--------------------------------//
//Prints weights
void Layer::printWeights() const{
    std::cout << this->weights.getString() << std::endl;
}

//Adds a Neuron to layer, always at last index
void Layer::addNeuron(){
    this->weights.addRow();
    this->weightGradients.addRow();
    this->bias.addRow();
    this->biasGradients.addRow();
    this->stimulations.addRow();
    this->numneurons++;
}

//Adds a weight to each Neuron in layer, always at last index
void Layer::addSynapse(){
    this->weights.addColumn();
    this->weightGradients.addColumn();
    this->inputs.addRow();
    this->numinputs++;
}

//Removes neuron at index "index" from layer
void Layer::removeNeuron(int index){
    this->weights.removeRow(index);
    this->weightGradients.removeRow(index);
    this->bias.removeRow(index);
    this->biasGradients.removeRow(index);
    this->stimulations.removeRow(index);
    this->numneurons--;
}

//Removes weight at index "index" from layer
void Layer::removeSynapse(int index){
    this->weights.removeColumn(index);
    this->weightGradients.removeColumn(index);
    this->inputs.removeRow(index);
    this->numinputs--;
}

//Saves everything important in layer to file:
//  numinputs, numneurons, and weights
void Layer::saveToFile(std::ofstream& file) const{
    file.write((char*)&this->numinputs, sizeof(float));
    file.write((char*)&this->numneurons, sizeof(float));
    this->weights.saveToFile(file);
    this->weightGradients.saveToFile(file);
    this->bias.saveToFile(file);
    this->biasGradients.saveToFile(file);
    this->inputs.saveToFile(file);
    this->stimulations.saveToFile(file);
}

//--------------------------------Setters and Getters--------------------------------//
//Sets the gradients to 0
void Layer::resetGradients(){
    for(int i = 0; i < this->weightGradients.getNRows(); i++){
        for(int j = 0; j < this->weightGradients.getNColumns(); j++){
            this->weightGradients.at(i,j) = 0;
            if(this->withbias) this->biasGradients.at(i,0) = 0;
        }
    }
}

//Gets magnitude^2 of the weight gradients for this layer
float Layer::getGradientMagnitude2(){
    float sum = 0;
    for(int i = 0; i < this->weightGradients.getNRows(); i++){
        for(int j = 0; j < this->weightGradients.getNColumns(); j++){
            sum += pow(this->weightGradients.at(i,j),2) + pow(this->biasGradients.at(i,0),2);
        }
    }
    return sum;
}

//Divides gradient by the magnitude given to normalize gradients throughout NeuralNet
void Layer::normalizeGradient(float magnitude){
    this->weightGradients = this->weightGradients/magnitude;
    if(this->withbias) this->biasGradients = this->biasGradients/magnitude;
}
