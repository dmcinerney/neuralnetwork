#include "Structure.hpp"

//--------------------------------Protected Functions--------------------------------//
//Cost function
//returns result
//Estimate are contained in the Structure member variable "estimate"
float Structure::cost(const Matrix& actual){
    float sum  = 0;
    for(int i = 0; i < this->estimate.getNRows(); i++){
        for(int j = 0; j < this->estimate.getNColumns(); j++){
            sum += .5*pow(this->estimate.at(i,j)-actual.at(i,j),2);
        }
    }
    return sum;
}

//--------------------------------Constructors--------------------------------//
Structure::Structure(int Ninputs, int Noutputs) :
        numinputs(Ninputs),
        numoutputs(Noutputs),
        estimate(Noutputs,1),
        note("Creating new structure"){
    std::cout << note << std::endl;
    std::cout << "for now this does nothing" << std::endl;
}

//Destructor
//For now, does nothing
Structure::~Structure(){
    
}
//--------------------------------Main Functions--------------------------------//
