#ifndef Structure_hpp
#define Structure_hpp

#include "Layer.hpp"

//Structure is a virtual class for a structure in the artificial brain.
//It is inherited by 2 classes: NeuralNet and Module.  Any subclass of
//this takes an input and produces an output and can be trained.
class Structure{
protected:
    //Number of inputs to the network
    int numinputs;
    
    //Number of outputs to the network
    int numoutputs;
    
    //estimate are the floats that come out of the structure
    //Used to store estimate from previous forward call in order to compute gradient
    Matrix estimate;
    
    //contains any possible notes one might want to be aware of when running to print to output
    std::string note;
    
//Protected functions used only internally by Structure classes
    //Cost function
    //returns result
    //Estimate are contained in the Structure member variable "estimate"
    float cost(const Matrix& actual);//NEED TO IMPLEMENT
    
    //Computes gradient of C with respect to all weights
    //Stores gradient for weights in each layer
    //Estimate are contained in the Structure member variable "estimate"
    virtual void costDerivative(const Matrix& actual) = 0;//NEED TO IMPLEMENT
    
    //Computes estimate of neural network from inputs
    //Stores estimate in Structure member variable "estimate"
    virtual void forward(const Matrix& inputs) = 0;//NEED TO IMPLEMENT
    
//Constructors
    //Default constructor
    Structure();//NEED TO IMPLEMENT
    
    //Copy constructor
    Structure(const Structure& structure);//NEED TO IMPLEMENT
    
    Structure(int Ninputs, int Noutputs);
    
    //Constructs Structure from file with given filename
    Structure(std::string filename);//NEED TO IMPLEMENT
    
    //Destructor
    //For now, does nothing
    ~Structure();//NEED TO IMPLEMENT
    
public:
//Functions used frequently
    //Trains structure
    //inputs - inputs to system, outputs - given correct outputs, numiterations - number of iterations of training
    virtual void train(const std::vector<Matrix>& inputs, const std::vector<Matrix>& outputs, int numiterations) throw(StructureException) = 0;//NEED TO IMPLEMENT
    
    //FIXME: may want other types of training such as semisupervised or unsupervised
    
    //Tests structure
    //inputs - inputs to system, outputs - given correct outputs
    //It will print out each example's input, estimate, output, and cost
    //predictiontype is a string that tells the neuralnet how to predict the outputs from the neural net's initial outputs
    virtual void test(const std::vector<Matrix>& inputs, const std::vector<Matrix>& outputs, std::string predictiontype) throw(StructureException) = 0;//NEED TO IMPLEMENT
    
    //Predicts a 1 or 0 based on if output is closer to 1 or 0
    //input - one input example
    //returns result - what it predicts
    virtual int predictBinary(const Matrix& input) throw(StructureException) = 0;//NEED TO IMPLEMENT
    
    //Save the structure to file
    //Very important so learning can continue throughout multiple runs
    virtual void saveToFile(std::string filename) const = 0;//NEED TO IMPLEMENT
    
//Other functions
    //Replaces note with a new note
    void replaceNote(std::string note){this->note = note;}
};

#endif /* Structure_hpp */
