#ifndef NeuralNet_hpp
#define NeuralNet_hpp

#include "Structure.hpp"

//FIXME: may want to add subclasses for different types of neural networks
//FIXME: may want to add a bias

//Neural Network inherits Structure. It consists of a series of layers.
//It takes an input and gives an estimate.  So far it can be trained just
//with back propogation.
class NeuralNet: public Structure{
protected:
//Member variables
    //vector of layers which is the makeup of the neural network
    std::vector<Layer> layers;
    
    //Scalar for the inputs
    float scalarin;
    
    //Scalar for the outputs
    float scalarout;
    
    float learningrate;
    
    int numstartingpoints;
    
//Private functions used only internally by NeuralNet class
    //Sets values for scalars for inputs and outputs
    void setscalars(const Matrix& inputs, const Matrix& outputs);//NEED TO IMPLEMENT
    
    //Updates weights baised on gradients stored in each layer
    void updateWeights(int Lrate);//NEED TO TEST FURTHER
    
    //Normalizes gradient so as to not make steps depend on magnitude, just direction
    void normalizeGradients();//NEED TO IMPLEMENT
    
//Private overriden virtual functions
    //Computes gradient of C with respect to all weights
    //Stores gradient for weights in each layer
    //Estimate are contained in Structure member variable "estimate"
    void costDerivative(const Matrix& actual) override;//NEED TO TEST FURTHER
    
    //Computes estimate of neural network from inputs
    //Stores estimate in member variable "estimate"
    void forward(const Matrix& inputs) override;//NEED TO TEST FURTHER
    
public:
//Friend functions
    friend void testNeuralNet();
    
//Constructors
    //Default constructor
    NeuralNet();
    
    //Copy constructor
    NeuralNet(const NeuralNet& neuralnet);
    
    //Create neural net with given specifications:
    //Nlayers - number of layers, Nnpl - number of neurons per layer (constant) (int), Ninputs - number of inputs, and Noutputs - number of outputs, Nstartingpoints - numnber of starting points
    NeuralNet(int Nlayers, int Nnpl, int Ninputs, int Noutputs, int Nstartingpoints);
    
    //Create neural net with given specifications:
    //Nlayers - number of layers, Nnpl - number of neurons per layer (varied) (int*), Ninputs - number of inputs, and Noutputs - number of outputs, Nstartingpoints - numnber of starting points
    NeuralNet(int Nlayers, int Nnpl[Nlayers-1], int Ninputs, int Noutputs, int Nstartingpoints);
    
    //Constructs NeuralNet from file with given filename
    NeuralNet(std::string filename);//NEED TO IMPLEMENT
    
    //Destructor
    //For now, does nothing
    ~NeuralNet();
    
//Functions used frequently
    //Trains neural network
    //inputs - inputs to system, outputs - given correct outputs, numiterations - number of iterations of training
    void train(const std::vector<Matrix>& inputs, const std::vector<Matrix>& outputs, int numiterations) throw(StructureException) override;//NEED TO IMPLEMENT
    
    //Tests neural network
    //inputs - inputs to system, outputs - given correct outputs
    //It will print out each example's input, estimate, output, and cost
    //predictiontype is a string that tells the neuralnet how to predict the outputs from the neural net's initial outputs
    void test(const std::vector<Matrix>& inputs, const std::vector<Matrix>& outputs, std::string predictiontype) throw(StructureException) override;//NEED TO IMPLEMENT
    
    //Predicts a 1 or 0 based on if output is closer to 1 or 0
    //input - one input example
    //returns result - what it predicts
    int predictBinary(const Matrix& input) throw(StructureException) override;//NEED TO IMPLEMENT
    
    //Sets the gradients in each layer to 0
    void resetGradients();
    
    //Save the neural network to file
    //Very important so learning can continue throughout multiple runs
    void saveToFile(std::string filename) const override;//NEED TO IMPLEMENT
    
//Other functions
    //Adds Neuron in layer layernum
    void addNeuron(int layernum);//NEED TO IMPLEMENT
    
    //Removes Neuron in layer layernum at index "index"
    void removeNeuron(int layernum, int index);//NEED TO IMPLEMENT
    
    //Prints the weights of layer layernum
    void printWeights(int layernum);//NEED TO IMPLEMENT
};

#endif /* NeuralNet_hpp */
