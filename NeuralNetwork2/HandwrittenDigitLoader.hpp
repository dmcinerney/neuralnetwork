#ifndef HandwrittenDigitLoader_hpp
#define HandwrittenDigitLoader_hpp

#include "NeuralNet.hpp"
#include "Module.hpp"
#include <iostream>

using namespace std;

void loadData(vector<Matrix>& inputs, vector<Matrix>& outputs);

#endif /* HandwrittenDigitLoader_hpp */
