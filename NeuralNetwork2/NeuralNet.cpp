#include "NeuralNet.hpp"
//--------------------------------Private functions--------------------------------//
//Updates weights baised on gradients stored in each layer
void NeuralNet::updateWeights(int Lrate){
    for(int i = 0; i < this->layers.size(); i++){
        this->layers[i].updateWeights(Lrate);
    }
}

//Sets the gradients in each layer to 0
void NeuralNet::resetGradients(){
    for(int i = 0; i < this->layers.size(); i++){
        this->layers[i].resetGradients();
    }
}

//Normalizes gradient so as to not make steps depend on magnitude, just direction
void NeuralNet::normalizeGradients(){
    float magnitude2 = 0;
    for(int i = 0; i < this->layers.size(); i++){
        magnitude2 += this->layers[i].getGradientMagnitude2();
    }
    float magnitude = sqrtf(magnitude2);
    //std::cout << magnitude << std::endl;
    if(magnitude == 0){
        return;
    }
    for(int i = 0; i < this->layers.size(); i++){
        this->layers[i].normalizeGradient(magnitude);
    }
}

//--------------------------------Private overriden functions--------------------------------//
//Computes gradient of C with respect to all weights
//Stores gradient for weights in each layer
//Estimate is contained in Structure member variable "estimate"
void NeuralNet::costDerivative(const Matrix& actual){
    Matrix errors = Matrix(this->estimate-actual);
    errors = Matrix(this->layers[this->layers.size()-1].calculateWeightGradient(errors));
    for(unsigned long i = this->layers.size()-2; i < this->layers.size()-1; i--){
        errors = Matrix(this->layers[i].calculateWeightGradient(errors));
    }
}

//Computes estimate of neural network from inputs
//Stores estimate in member variable "estimate"
void NeuralNet::forward(const Matrix& inputs){
    Matrix temp = Matrix(inputs);
    for(int i = 0; i < this->layers.size(); i++){
        temp = this->layers[i].getOutput(temp);
    }
    this->estimate = temp;
}

//--------------------------------Constructors--------------------------------//
//Create neural net with given specifications:
//Nlayers - number of layers, Nnpl - number of neurons per layer (constant) (int), Ninputs - number of inputs, and Noutputs - number of outputs, Nstartingpoints - numnber of starting points
NeuralNet::NeuralNet(int Nlayers, int Nnpl, int Ninputs, int Noutputs, int Nstartingpoints) :
Structure(Ninputs,Noutputs),
scalarin(.0001),
scalarout(1),
learningrate(1),
numstartingpoints(Nstartingpoints){
    replaceNote("This structure is a NeuralNet");
    std::cout << this->note << std::endl;
    this->layers = std::vector<Layer>(Nlayers);
    if(Nlayers < 1){
        //FIXME: change to exception
        exit(1);
    } else if(Nlayers == 1){
        this->layers[0] = Layer(Ninputs, Noutputs);
    } else {
        this->layers[0] = Layer(Ninputs, Nnpl);
        for(int i = 1; i < Nlayers-1; i++){
            this->layers[i] = Layer(Nnpl,Nnpl);
        }
        this->layers[Nlayers-1] = Layer(Nnpl,Noutputs);
    }
}

//Create neural net with given specifications:
//Nlayers - number of layers, Nnpl - number of neurons per layer (varied) (int*), Ninputs - number of inputs, and Noutputs - number of outputs, Nstartingpoints - numnber of starting points
NeuralNet::NeuralNet(int Nlayers, int Nnpl[Nlayers-1], int Ninputs, int Noutputs, int Nstartingpoints) :
Structure(Ninputs,Noutputs),
scalarin(.0001),
scalarout(1),
learningrate(1),
numstartingpoints(Nstartingpoints){
    replaceNote("This structure is a NeuralNet");
    std::cout << this->note << std::endl;
    this->layers = std::vector<Layer>(Nlayers);
    if(Nlayers < 1){
        //FIXME: change to exception
        exit(1);
    } else if(Nlayers == 1){
        this->layers[0] = Layer(Ninputs, Noutputs);
    } else {
        this->layers[0] = Layer(Ninputs, Nnpl[0]);
        for(int i = 1; i < Nlayers-1; i++){
            this->layers[i] = Layer(Nnpl[i-1],Nnpl[i]);
        }
        this->layers[Nlayers-1] = Layer(Nnpl[Nlayers-2],Noutputs);
    }
}

//Destructor
//For now, does nothing
NeuralNet::~NeuralNet(){
    
}

//--------------------------------Main functions--------------------------------//
//Trains neural network
//inputs - inputs to system, outputs - given correct outputs, numiterations - number of iterations of training
void NeuralNet::train(const std::vector<Matrix>& inputs, const std::vector<Matrix>& outputs, int numiterations) throw(StructureException){
    if(this->numstartingpoints > 1){
        std::cout << "Can't do more than 1 starting point right now." << std::endl;//FIXME: implement this
        throw StructureException();
    }
    for(int n = 0; n < this->numstartingpoints; n++){
        float lr = this->learningrate;
        //lr = 1;
        for(int iterationnum = 0; iterationnum < numiterations; iterationnum++){
            this->resetGradients();
            for(int i = 0; i < inputs.size(); i++){
                this->forward(inputs[i]);
                this->costDerivative(outputs[i]);
                if(((i+1) % 100) == 0 && (i+1) != inputs.size()){
                    std::cout << i+1 << " / " << inputs.size() << std::endl;
                }
                if(((i+1) % 1) == 0 && (i+1) != inputs.size()){
                    this->normalizeGradients();
                    this->updateWeights(lr);
                    this->resetGradients();
                }
            }
            std::cout << inputs.size() << " / " << inputs.size() << std::endl;
            this->normalizeGradients();
            this->updateWeights(lr);
            lr = lr*1;//FIXME: get suggestions about learning rate
            if((iterationnum+1) % 1 == 0 && iterationnum+1 != numiterations){
                std::cout << "Iteration " << iterationnum+1 << " / " << numiterations << std::endl;
            }
        }
        std::cout << numiterations << " / " << numiterations << std::endl;
    }
}

//Tests neural network
//inputs - inputs to system, outputs - given correct outputs
//It will print out each example's input, estimate, output, and cost
//predictiontype is a string that tells the neuralnet how to predict the outputs from the neural net's initial outputs
void NeuralNet::test(const std::vector<Matrix>& inputs, const std::vector<Matrix>& outputs, std::string predictiontype) throw(StructureException){
    int numcorrect = 0;
    for(int i = 0; i < inputs.size(); i++){
        if(predictiontype == "binary"){
            if(outputs[i].getNRows() != 1){
                std::cout << "too many outputs given" << std::endl;
                throw StructureException();
            }
            if(outputs[i].at(0,0) == this->predictBinary(inputs[i])){
                numcorrect++;
            }
        } else if(predictiontype == "digitsbinary"){//maybe should add exception to check if output is binary
            this->forward(inputs[i]);
            bool equal = true;
            for(int j = 0; j < estimate.getNRows(); j++){
                if(outputs[i].at(j,0) != (float)(this->estimate.at(j,0)>=.5)){
                    equal = false;
                    break;
                }
            }
            if(equal){
                numcorrect++;
            }
        } else {
            std::cout << predictiontype << " is not a valid prediction type." << std::endl;
            throw StructureException();
        }
    }
    float accuracy = ((float) numcorrect)/inputs.size();
    std::cout << "Accuracy is " << accuracy << " (" << numcorrect << "/" << inputs.size() << ")." << std::endl;
}

//Predicts a 1 or 0 based on if output is closer to 1 or 0
//input - one input example
//returns result - what it predicts
int NeuralNet::predictBinary(const Matrix& input) throw(StructureException){
    if(this->numoutputs != 1){
        std::cout << "too many outputs from neural net" << std::endl;
        throw StructureException();
    }
    this->forward(input);
    if(this->estimate.at(0, 0) >= .5){
        return 1;
    } else {
        return 0;
    }
}

//Save the neural network to file
//Very important so learning can continue throughout multiple runs
void NeuralNet::saveToFile(std::string filename) const{
    
}

//--------------------------------Setters and Getters--------------------------------//
