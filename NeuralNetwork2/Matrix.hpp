#ifndef Matrix_hpp
#define Matrix_hpp

#include <iostream>
#include <vector>
#include <fstream>
#include "Exceptions.hpp"

//The Matrix class is a container for a matrix of floats.
class Matrix{
private:
//Member variables
    //Stores all values in matrix
    //FIXME: May want to use different containter
    std::vector< std::vector<float> > values;
    
    //number of rows
    int numrows;
    
    //number of columns
    int numcolumns;
    
//Private functions used only internally by Matrix class
    //Used to initialize values for matrix
    //If Matrix pointer is null, initialize all values to 0
    //If Matrix pointer is not null, then initialize all values
    //  to the corresponding values in matrix
    void initialize(const Matrix* matrix);
    
    //Checks if the input row and column are out of the bounds of the matrix
    //Throws an error if they are out of bounds
    void checkOutOfBounds(int row, int column) const throw(MatrixException);
    
public:
//Friend functions
    friend void testMatrix();
    
//Constructors
    //Default constructor
    Matrix();
    
    //Copy constructor
    Matrix(const Matrix& matrix);
    
    //Constructs matrix with given specifications
    //Nrows - number of rows, Ncolumns - number of columns
    Matrix(int Nrows, int Ncolumns);
    
    //Constructs matrix from file
    Matrix(std::ifstream& file);
    
    //Destructor
    //For now, does nothing
    ~Matrix();
    
//Functions used frequently
    //Loops over all elements and performs function(element, number) and
    //puts results into a matrix it returns by value
    Matrix forAllElements(const float number, float (*function)(const float&, const float&)) const;
    
    //Loops over all elements and performs function(element, element of matrix parameter) and
    //puts results into a matrix it returns by value
    //Throws an error if the input matrix doesn't have the same dimensions as this
    Matrix forAllElements(const Matrix& m, float (*function)(const float&, const float&)) const throw(MatrixException);
    
    //Gets float at given row and column
    //returns float by const reference
    //Neither the function itself or that which calls it can change object
    //Throws an error if row or column are out of bounds
    const float& at(int row, int column) const throw(MatrixException);
    
    //Gets float at given row and column
    //returns float by reference
    //The function that calls it may change object using the returned reference
    //Throws an error if row or column are out of bounds
    float& at(int row, int column) throw(MatrixException);
    
    //Element-wise addition
    //Throws an error if input matrix is not the same dimensions as this
    Matrix operator+(const Matrix& m) const throw(MatrixException);
    
    //Element-wise subtraction
    //Throws an error if input matrix is not the same dimensions as this
    Matrix operator-(const Matrix& m) const throw(MatrixException);
    
    //Matrix multiplication
    //Throws an error if input matrix is not the same dimensions as this
    Matrix dot(const Matrix& m) const throw(MatrixException);
    
    //Element-wise multiplication
    //Throws an error if dimensions of input matrix are not compatible
    Matrix multiply(const Matrix& m) const throw(MatrixException);
    
    //Element-wise division
    //Throws an error if dimensions of input matrix are not compatible
    Matrix divide(const Matrix& m) const throw(MatrixException);
    
    //Scalar multiplication
    Matrix operator*(const float scaler) const;
    
    //Scalar division
    Matrix operator/(const float scaler) const;
    
    //could add more of these operations easily, but
    //these are the only ones needed as of now
    
//Other functions
    //Adds row at end of matrix
    void addRow();
    
    //Adds column at end of matrix
    void addColumn();
    
    //Removes row at given index
    //Throws an error if index is out of bounds
    void removeRow(int index) throw(MatrixException);
    
    //Removes column at given index
    //Throws an error if index is out of bounds
    void removeColumn(int index) throw(MatrixException);
    
    //Saves all member variables to file
    void saveToFile(std::ofstream& filename) const;
    
//Setters and Getters
    //Gets number of rows
    int getNRows() const{return numrows;}
    
    //Gets number of columns
    int getNColumns() const{return numcolumns;}
    
    //Creates transpose of matrix
    //returns by value
    Matrix getTranspose() const;
    
    //Gets string printout of whole matrix
    std::string getString() const;
};

//Non-member functions
//Float addition
float addition(const float& x, const float& y);

//Float subtraction
float subtraction(const float& x, const float& y);

//Float multiplication
float multiplication(const float& x, const float& y);

//Float division
float division(const float& x, const float& y);

#endif /* Matrix_hpp */