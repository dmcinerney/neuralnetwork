# README #


### What is this repository for? ###

This repository has code for a neural network that will eventually be generalized to work for regression as well as classification.  Currently, it only works well for binary classification.

### How do I get set up? ###

This project is still under development, but it can be run from a C++ file by including NueralNet header file and using it's NeuralNet class.  In order to train the neural net, one needs to create a NueralNet object and use the train function where the inputs and outputs are passed as arguments in the form of vectors of Matrices.  For now, these Matrices need to be in the form of a Matrix object in the Matrix header file.

NOTE: Soon this code will be refactored so that the user can pass inputs as CSV files and the code itself will load the data into matrices.

### How do I run the tests? ###

The makefile makes an executable that runs a neural network test.  This test runs the network on some of the samples from a handwritten digit recognition dataset taken from https://www.kaggle.com/c/digit-recognizer/data.  The test takes only 0's and 1's from the dataset and trains the neural network to classify which one is which.  It tests this on dev data, which it splits up itself from the training and outputs the accuracy at the end (the last accuracy outputted).

To run these tests, just type:

$ make

and then:

$ ./NeuralNet

There are more notes in the README in the NeuralNet folder.


You can reach the owner of this repo, Jered McInerney, at dmciner1@jhu.edu.